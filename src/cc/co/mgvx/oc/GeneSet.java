/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A set of <code>Gene</code>s from a certain group.
 * @author Marius Gavrilescu
 */
public final class GeneSet {
    /** A <code>SortedMap</code> of the <code>Gene</code>s in this set */
    private final SortedMap<Gene,Long> mset=new TreeMap<Gene, Long>();
    /** The group in which the <code>Gene</code>s in this set belong*/
    private final String group;
    /** The count of <code>Gene</code>s in this set */
    private int grandTotal=0;
    /**
     * Default constructor for <code>GeneSet</code>
     * @param group the group of the GeneSet
     */
    public GeneSet(final String group) {
	this.group = group;
    }

    /**
     * Adds <code>count</code> copies of <code>gene</code> in this <code>GeneSet</code>
     * @param gene what <code>Gene</code> to add
     * @param count number of times <code>gene</code> should be added
     */
    public void add(final Gene gene, final long count){
	final Long cnt=this.mset.get(gene);
	this.mset.put(gene, count+(cnt==null?0:cnt.longValue()));
	this.grandTotal+=count;
    }
    /**
     * Runs a monohybridization.
     * It adds <code>count</code> times each of the four <code>Gene</code>ss
     * @param gene1 the first gene
     * @param gene2 the second gene
     * @param count how many times to add each result
     */
    private void monohybridize(final Gene gene1, final Gene gene2, final long count){
	add(new Gene(gene1.left ,gene2.left ,gene1.targets&gene2.targets,this.group,false), count);
	add(new Gene(gene1.left ,gene2.right,gene1.targets&gene2.targets,this.group,false), count);
	add(new Gene(gene1.right,gene2.left ,gene1.targets&gene2.targets,this.group,false), count);
	add(new Gene(gene1.right,gene2.right,gene1.targets&gene2.targets,this.group,false), count);
    }
    /**
     * Runs a monohybridization on each pair of two <code>Gene</code>s
     */
    public void hybridize(){
	final Gene[] genes=new Gene[this.grandTotal];
	int pos=0;
	for (Map.Entry<Gene, Long> ent : this.mset.entrySet()) {
	    long cnt=ent.getValue();
	    for(;cnt>0;cnt--){
		genes[pos]=ent.getKey();
		pos++;
	    }
	}
	final SortedMap<Gene,Long> mmset=new TreeMap<Gene, Long>(this.mset);
	this.mset.clear();
	this.grandTotal=0;
	for(int i=0;i<genes.length;i++)
	    for(int j=i+1;j<genes.length;j++){
		final Gene gene1=genes[i];
		final Gene gene2=genes[j];
		monohybridize(gene1,gene2,mmset.get(gene1).longValue()*mmset.get(gene2).longValue());
	    }
    }
    /**
     * Returns a map containing all the <code>Gene</code>s in this <code>GeneSet</code> and their ratio, expressed as percentages
     * @return the map
     */
    public Map<Gene,Float> getRatios(){
	final HashMap<Gene, Float> ans=new HashMap<Gene, Float>(this.mset.size());
	for (Gene gene : this.mset.keySet())
	    ans.put(gene, 100f*this.mset.get(gene)/this.grandTotal);
	return ans;
    }
}
