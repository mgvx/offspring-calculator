/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.awt.FlowLayout;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A <code>JPanel</code> containing <code>JComboBox</code>es for each <code>Gene</code> which applies to a certain target
 * @author Marius Gavrilescu
 */
public final class TraitPanel extends JPanel{
    /**
     * Only constructor for <code>TraitPanel</code>
     * Creates a <code>TraitPanel</code> for a certain target
     * @param target the target of this <code>TraitPanel</code>
     */
    public TraitPanel(final long target) {
	setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	for (Entry<String, Set<Gene>> set : Gene.ALL_GENES.entrySet()) {
	    final JComboBox combo=new JComboBox(); //NOPMD
	    final JLabel label=new JLabel(set.getKey()+": "); //NOPMD
	    final JPanel panel=new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5)); //NOPMD
	    boolean empty=true;
	    for (Gene gene : set.getValue())
		if((gene.targets&target)>0){
		    combo.addItem(gene);
		    empty=false;
		}
	    if(!empty){
		panel.add(label);
		panel.add(combo);
		add(panel);
	    }
	}
	revalidate();
    }

}
