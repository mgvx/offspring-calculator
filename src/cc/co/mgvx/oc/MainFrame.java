/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.SplashScreen;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

/**
 * The main JFrame of this app
 * @author Marius Gavrilescu
 */
public final class MainFrame extends javax.swing.JFrame {

    /** Creates new form MainFrame */
    public MainFrame() {

	//BEGIN kludge to set 'insets' of JFrame
	final JPanel kludgePanel=new JPanel(new BorderLayout());
	kludgePanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(kludgePanel);
	//END kludge

        initComponents();
	genSpinner.setVisible(false); //useless
    }
    /** A <code>TraitPanel</code> targeting females */
    private final JPanel femalePanel=new TraitPanel(Gene.TARGET_FEMALE);
    /** A <code>TraitPanel</code> targeting males */
    private final JPanel malePanel=new TraitPanel(Gene.TARGET_MALE);

    /** Initializes components. Machine-generated. */
    @SuppressWarnings({"unchecked","PMD"})
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        final JPanel containerPanel = new JPanel(); // NOI18N
	final JPanel femaleContainerPanel = new JPanel();
	final JLabel femaleLabel = new JLabel();
	final JSeparator femaleSeparator = new JSeparator();
	final JPanel maleContainerPanel = new JPanel();
	final JLabel maleLabel = new JLabel();
	final JSeparator maleSeparator = new JSeparator();
	final JPanel centerPanel = new JPanel();
	final JLabel xLabel = new JLabel();
	final JPanel bottomContainerPanel = new JPanel();
	final JButton runButton = new JButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	ResourceBundle bundle = ResourceBundle.getBundle("cc/co/mgvx/oc/Bundle");
        setTitle(bundle.getString("oc")); // NOI18N
        setResizable(false);

        containerPanel.setLayout(new BorderLayout());

        femaleContainerPanel.setBorder(BorderFactory.createEtchedBorder());
        femaleContainerPanel.setLayout(new BoxLayout(femaleContainerPanel, BoxLayout.Y_AXIS));

        femaleLabel.setFont(new Font("Dialog", 0, 24)); // NOI18N
        femaleLabel.setLabelFor(femaleContainerPanel);
        femaleLabel.setText(bundle.getString("female")); // NOI18N
        femaleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        femaleLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        femaleContainerPanel.add(femaleLabel);
        femaleContainerPanel.add(femaleSeparator);

        femaleContainerPanel.add(femalePanel);
        containerPanel.add(femaleContainerPanel, BorderLayout.LINE_START);

        maleContainerPanel.setBorder(BorderFactory.createEtchedBorder());
        maleContainerPanel.setLayout(new BoxLayout(maleContainerPanel, BoxLayout.Y_AXIS));

        maleLabel.setFont(new Font("Dialog", 0, 24)); // NOI18N
        maleLabel.setLabelFor(maleContainerPanel);
        maleLabel.setText(bundle.getString("male")); // NOI18N
        maleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        maleLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        maleContainerPanel.add(maleLabel);
        maleContainerPanel.add(maleSeparator);

        maleContainerPanel.add(malePanel);
        containerPanel.add(maleContainerPanel, BorderLayout.LINE_END);

        centerPanel.setLayout(new BorderLayout());

        xLabel.setFont(new Font("Dialog", 1, 48));
        xLabel.setHorizontalAlignment(SwingConstants.CENTER);
        xLabel.setText(" X ");
        centerPanel.add(xLabel, BorderLayout.CENTER);

        containerPanel.add(centerPanel, BorderLayout.CENTER);

        getContentPane().add(containerPanel, BorderLayout.CENTER);

        genSpinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
        bottomContainerPanel.add(genSpinner);

        runButton.setText(bundle.getString("run")); // NOI18N
        runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                runButtonActionPerformed(evt);
            }
        });
        bottomContainerPanel.add(runButton);

        getContentPane().add(bottomContainerPanel, BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings({"PMD","element-type-mismatch"})
    private void runButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_runButtonActionPerformed
	final Map<String,GeneSet> map=new HashMap<String,GeneSet>(10);
	final ResourceBundle bundle=ResourceBundle.getBundle("cc/co/mgvx/oc/Bundle");
	final JDialog dialog=new JDialog(this, bundle.getString("results"), true);
	final JPanel contentPane=new JPanel();
	contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
	for(Component com: femalePanel.getComponents()){
	    final JPanel panel=(JPanel)com;
	    final String group=((JLabel)panel.getComponent(0)).getText();
	    final Gene gene=(Gene)((JComboBox)panel.getComponent(1)).getSelectedItem();
	    GeneSet geneset=map.get(group);
	    if(geneset==null){
		geneset=new GeneSet(group);
		map.put(group, geneset);
	    }
	    geneset.add(gene, 1);
	}
	for(Component com: malePanel.getComponents()){
	    final JPanel panel=(JPanel)com;
	    final String group=((JLabel)panel.getComponent(0)).getText();
	    final Gene gene=(Gene)((JComboBox)panel.getComponent(1)).getSelectedItem();
	    GeneSet geneset=map.get(group);
	    if(geneset==null){
		geneset=new GeneSet(group);
		map.put(group, geneset);
	    }
	    geneset.add(gene, 1);
	}
	for (Map.Entry<String, GeneSet> ent : map.entrySet()) {
	    final String group=ent.getKey();
	    final GeneSet set=ent.getValue();
	    final JLabel label=new JLabel(group);
	    label.setFont(new Font(Font.DIALOG, Font.BOLD, 12));
	    contentPane.add(label);
	    for(int i=0;i<(Integer)genSpinner.getValue();i++)
		set.hybridize();
	    final Map<Gene,Float> results = set.getRatios();
	    for (Map.Entry<Gene, Float> entry : results.entrySet()) {
		final Gene theGene=entry.getKey();
		final Float theProbability=entry.getValue();
		final JLabel lab=new JLabel(String.format("<html>%.2f%% %s</html>", theProbability,theGene.toNonHtmlString()));
		contentPane.add(lab);
	    }
	    contentPane.add(new JSeparator());
	}
	final JScrollPane scrollPane=new JScrollPane(contentPane);
	dialog.setContentPane(scrollPane);
	dialog.pack();
	dialog.setSize(dialog.getSize().width+10, Toolkit.getDefaultToolkit().getScreenSize().height-20);
	dialog.setVisible(true);
    }//GEN-LAST:event_runButtonActionPerformed

    /**
     * The main function
     * @param args the command line arguments
     */
    public static void main(final String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {//NOPMD
            //do nothing
        }
        //</editor-fold>

	Gene.ALL_GENES.size(); //KLUDGE KLUDGE KLUDGE. Runs <clinit> on Gene

        EventQueue.invokeLater(new Runnable() {
	    @Override
            public void run() {
                final MainFrame frame=new MainFrame();
		final Preferences prefs=Preferences.userNodeForPackage(MainFrame.class);
		if(prefs.getBoolean("first", true))
		    try{
			final JDialog dialog=new JDialog((Frame)null, "License", true);
			dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			final JLabel label=new JLabel("This program is licensed under the GNU GPL. Full text follows");
			label.setAlignmentX(Component.CENTER_ALIGNMENT);
			final JEditorPane pane=new JEditorPane(MainFrame.class.getResource("COPYING"));
			final JScrollPane scpane=new JScrollPane(pane);
			final JPanel contentPane=new JPanel();
			final JButton closeButton=new JButton("OK");
			closeButton.addActionListener(new ActionListener() {
			    @Override
			    public void actionPerformed(final ActionEvent arg0) {
				dialog.dispose();
			    }
			});
			closeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
			contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
			pane.setEditable(false);
			contentPane.add(label);
			contentPane.add(scpane);
			contentPane.add(closeButton);
			dialog.setContentPane(contentPane);
			dialog.setSize(500, 500);
			dialog.setVisible(true);
			prefs.putBoolean("first", false);
			try {
			    prefs.flush();
			} catch (BackingStoreException ex) {
			    ex.printStackTrace();
			}
		    }catch(IOException ex){
			ex.printStackTrace();
		    }
		final SplashScreen screen=SplashScreen.getSplashScreen();
		if(screen!=null)
		    screen.close();
		frame.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JSpinner genSpinner = new JSpinner();
    // End of variables declaration//GEN-END:variables
}
