# Repo contents
The source files are in `src/`

`manifest.mf` is the skeleton manifest of the jar.

Binaries can be found under **Downloads**. Each jar also contains the sources

You can import the project into [NetBeans](http://netbeans.org) to edit and build it easily
